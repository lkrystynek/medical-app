import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { rootRouterConfig } from './app.routes';

import { AppComponent } from './app.component';
import { PatientComponent } from './patient/patient.component';
import { ListComponent } from './list/list.component';


@NgModule({
  declarations: [
    AppComponent,
    PatientComponent,
    ListComponent,
        
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    rootRouterConfig
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
