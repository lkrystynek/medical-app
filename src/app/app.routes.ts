import { Routes, RouterModule } from '@angular/router';

import { ListComponent } from './list/list.component';
import { PatientComponent } from './patient/patient.component';

const APP_ROUTES: Routes = [
  { path: '', redirectTo: 'list', pathMatch: 'full' },
  { path: 'list', component: ListComponent },
  { path: 'patient/:patientNo',  component: PatientComponent }
];

export const rootRouterConfig = RouterModule.forRoot(APP_ROUTES);

