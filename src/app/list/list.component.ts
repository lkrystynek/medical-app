import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from '../app.component'

import { ClPatients } from '../patients-list';
import { ClInstitution } from '../institutions-list';

const PATIENTS: ClPatients[] =
  [{
    institute: 'I1',
    department: 'D1',
    patientNumber: 1000001,
    firstName: "Tom",
    lastName: "Hanks",
    dateOfBirth: "1956-07-09",
    gender: "Male",
    medicationName: "Aspirin",
    dosage: "50mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I1',
    department: 'D1',
    patientNumber: 1000002,
    firstName: "Brad",
    lastName: "Pit",
    dateOfBirth: "1963-12-18",
    gender: "Male",
    medicationName: "Claritin",
    dosage: "50mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I1',
    department: 'D2',
    patientNumber: 1000003,
    firstName: "Will",
    lastName: "Smith",
    dateOfBirth: "1968-09-25",
    gender: "Male",
    medicationName: "Heparin",
    dosage: "50mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I1',
    department: 'D2',
    patientNumber: 1000004,
    firstName: "Johnny",
    lastName: "Depp",
    dateOfBirth: "1963-06-09",
    gender: "Male",
    medicationName: "Ibuprofen",
    dosage: "400mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I1',
    department: 'D3',
    patientNumber: 1000005,
    firstName: "George",
    lastName: "Clooney",
    dateOfBirth: "1961-05-06",
    gender: "Male",
    medicationName: "Penicilin",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I1',
    department: 'D3',
    patientNumber: 1000006,
    firstName: "Jennifer",
    lastName: "Lawrence",
    dateOfBirth: "1990-08-15",
    gender: "Female",
    medicationName: "Duomox",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I2',
    department: 'D1',
    patientNumber: 1000007,
    firstName: "Mila",
    lastName: "Kunis",
    dateOfBirth: "1983-08-14",
    gender: "Female",
    medicationName: "azithromycin",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I2',
    department: 'D1',
    patientNumber: 1000008,
    firstName: "Julia",
    lastName: "Roberts",
    dateOfBirth: "1967-10-28",
    gender: "Female",
    medicationName: "clindamycin",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I2',
    department: 'D2',
    patientNumber: 1000009,
    firstName: "Jennifer",
    lastName: "Aniston",
    dateOfBirth: "1969-02-11",
    gender: "Female",
    medicationName: "cephalexin",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I2',
    department: 'D2',
    patientNumber: 1000010,
    firstName: "Jennifer",
    lastName: "Lopez",
    dateOfBirth: "1969-07-24",
    gender: "Female",
    medicationName: "amoxicillin",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I2',
    department: 'D3',
    patientNumber: 1000011,
    firstName: "Sandra",
    lastName: "Bullock",
    dateOfBirth: "1964-07-26",
    gender: "Female",
    medicationName: "amoxicillin",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I2',
    department: 'D3',
    patientNumber: 1000012,
    firstName: "Gwyneth",
    lastName: "Paltrow",
    dateOfBirth: "1972-09-27",
    gender: "Female",
    medicationName: "amoxicillin",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  }];

  const INSTITUTIONS: ClInstitution[] = [
  { code: 'I1', name: 'St. John\'s Institution' },
  { code: 'I2', name: 'St. Peter\'s Institution' },
  { code: 'I3', name: 'St. Andrew\'s Institution' }
];

const DEPARTMENTS: ClInstitution[] = [
  { code: 'D1', name: 'Hematology' },
  { code: 'D2', name: 'Radiology' },
  { code: 'D3', name: 'Emergency' }
];


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html'

})

export class ListComponent {

  institutions = INSTITUTIONS;
  selectedInstitution: ClInstitution;

  departments = DEPARTMENTS;
  selectedDepartment: ClInstitution;

  
  patients = PATIENTS;
  passed = [];
  //passed = PATIENTS; //replace above line for listing all patients on page - design purpose

  vm = {
    iCode: '',
    dCode: ''
  };

  
  onSubmit() {
    var selectedInstitute : string= this.vm.iCode;
    var selectedDepartment : string= this.vm.dCode;

    this.passed = this.patients.filter(function(patient){return patient.institute == selectedInstitute && patient.department == selectedDepartment}); 
        
    console.log(this.vm);
  }
}





