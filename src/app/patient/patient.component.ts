import {Component, OnInit}       from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ClPatients } from '../patients-list';

const PATIENTS: ClPatients[] =
  [{
    institute: 'I1',
    department: 'D1',
    patientNumber: 1000001,
    firstName: "Tom",
    lastName: "Hanks",
    dateOfBirth: "1956-07-09",
    gender: "Male",
    medicationName: "Aspirin",
    dosage: "50mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I1',
    department: 'D1',
    patientNumber: 1000002,
    firstName: "Brad",
    lastName: "Pit",
    dateOfBirth: "1963-12-18",
    gender: "Male",
    medicationName: "Claritin",
    dosage: "50mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I1',
    department: 'D2',
    patientNumber: 1000003,
    firstName: "Will",
    lastName: "Smith",
    dateOfBirth: "1968-09-25",
    gender: "Male",
    medicationName: "Heparin",
    dosage: "50mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I1',
    department: 'D2',
    patientNumber: 1000004,
    firstName: "Johnny",
    lastName: "Depp",
    dateOfBirth: "1963-06-09",
    gender: "Male",
    medicationName: "Ibuprofen",
    dosage: "400mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I1',
    department: 'D3',
    patientNumber: 1000005,
    firstName: "George",
    lastName: "Clooney",
    dateOfBirth: "1961-05-06",
    gender: "Male",
    medicationName: "Penicilin",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I1',
    department: 'D3',
    patientNumber: 1000006,
    firstName: "Jennifer",
    lastName: "Lawrence",
    dateOfBirth: "1990-08-15",
    gender: "Female",
    medicationName: "Duomox",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I2',
    department: 'D1',
    patientNumber: 1000007,
    firstName: "Mila",
    lastName: "Kunis",
    dateOfBirth: "1983-08-14",
    gender: "Female",
    medicationName: "azithromycin",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I2',
    department: 'D1',
    patientNumber: 1000008,
    firstName: "Julia",
    lastName: "Roberts",
    dateOfBirth: "1967-10-28",
    gender: "Female",
    medicationName: "clindamycin",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I2',
    department: 'D2',
    patientNumber: 1000009,
    firstName: "Jennifer",
    lastName: "Aniston",
    dateOfBirth: "1969-02-11",
    gender: "Female",
    medicationName: "cephalexin",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I2',
    department: 'D2',
    patientNumber: 1000010,
    firstName: "Jennifer",
    lastName: "Lopez",
    dateOfBirth: "1969-07-24",
    gender: "Female",
    medicationName: "amoxicillin",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I2',
    department: 'D3',
    patientNumber: 1000011,
    firstName: "Sandra",
    lastName: "Bullock",
    dateOfBirth: "1964-07-26",
    gender: "Female",
    medicationName: "amoxicillin",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  },
  {
    institute: 'I2',
    department: 'D3',
    patientNumber: 1000012,
    firstName: "Gwyneth",
    lastName: "Paltrow",
    dateOfBirth: "1972-09-27",
    gender: "Female",
    medicationName: "amoxicillin",
    dosage: "500mg",
    time: "2017-02-04T09:30",
    remark: 'Some Information'
  }];


@Component({
  
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  //providers: [PatientListServiece]
})

export class PatientComponent {
    
    patientNo: string;
    passed = [];
    patients = PATIENTS;
     
    
    constructor(private router: Router, private ActivatedRoute: ActivatedRoute) {
        this.patientNo = ActivatedRoute.snapshot.params['patientNo'];
    }
      
    ngOnInit(){
        var selectedPatient : number = +this.patientNo;
        console.log(selectedPatient);
        this.passed = this.patients.filter(function(patient){return patient.patientNumber == selectedPatient});
        console.log(this.passed);
    }
}

