export class ClPatients {
  institute: string;
  department: string;
  patientNumber: number;
  firstName: string;
  lastName: string;
  dateOfBirth: string;
  gender: string;
  medicationName: string;
  dosage: string;
  time: string;
  remark: string;
};


